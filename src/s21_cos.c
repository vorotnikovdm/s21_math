#include "s21_math.h"

long double s21_cos(double x) {
  long double res = 0.0;
  if (x == 0) {
    res = 1;
  } else if (x == S21_INF || x == S21_NINF || x != x) {
    res = S21_NAN;
  } else {
    res = s21_sin(S21_PI / 2 - x);
  }
  return res;
}
