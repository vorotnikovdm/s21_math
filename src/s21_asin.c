#include "s21_math.h"

long double s21_asin(double x) {
  long double res = x;
  if (x == 1) {
    res = S21_PI_2;
  } else if (x == -1) {
    res = -S21_PI_2;
  } else {
    long double temp = x / (s21_sqrt(1 - x * x));
    res = s21_atan(temp);
  }
  return res;
}
