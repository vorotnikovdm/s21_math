#include "s21_math.h"

long double s21_fabs(double x) {
  double_num number;
  number.num = x;
  if (number.bits.s == 1 && x == 0) {
    x *= -1;
  }
  return x < 0 ? -x : x;
}
