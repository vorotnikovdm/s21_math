#include "s21_test_math.h"

START_TEST(test_tan_positive_double) {
  ck_assert_ldouble_eq_tol(s21_tan(S21_PI), tanl(S21_PI), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_tan(0.25 * S21_PI), tanl(0.25 * S21_PI),
                           S21_EPS);
  ck_assert_ldouble_eq_tol(s21_tan(0.77 * S21_PI), tanl(0.77 * S21_PI),
                           S21_EPS);
  ck_assert_ldouble_eq_tol(s21_tan(2 * S21_PI), tanl(2 * S21_PI), S21_EPS);
}
END_TEST

START_TEST(test_tan_negative_double) {
  ck_assert_ldouble_eq_tol(s21_tan(-S21_PI), tanl(-S21_PI), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_tan(-0.25 * S21_PI), tanl(-0.25 * S21_PI),
                           S21_EPS);
  ck_assert_ldouble_eq_tol(s21_tan(-0.77 * S21_PI), tanl(-0.77 * S21_PI),
                           S21_EPS);
  ck_assert_ldouble_eq_tol(s21_tan(-2 * S21_PI), tanl(-2 * S21_PI), S21_EPS);
}
END_TEST

START_TEST(test_tan_specific_value) {
  ck_assert_ldouble_eq_tol(s21_tan(0.0), tanl(0.0), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_tan(-0.0), tanl(-0.0), S21_EPS);
  ck_assert(isnan(s21_tan(S21_NAN)) == 1);
  ck_assert(isnan(s21_tan(S21_INF)) == 1);
  ck_assert(isnan(s21_tan(S21_NINF)) == 1);
}
END_TEST

Suite *s21_test_tan() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_tan");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_tan_positive_double);
  tcase_add_test(tc_core, test_tan_negative_double);
  tcase_add_test(tc_core, test_tan_specific_value);

  suite_add_tcase(s, tc_core);
  return s;
}
