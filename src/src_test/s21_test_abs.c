#include "s21_test_math.h"

START_TEST(test_abs_positive_int) {
  ck_assert_int_eq(s21_abs(1), abs(1));
  ck_assert_int_eq(s21_abs(17), abs(17));
  ck_assert_int_eq(s21_abs(26), abs(26));
  ck_assert_int_eq(s21_abs(50), abs(50));
  ck_assert_int_eq(s21_abs(96), abs(96));
}
END_TEST

START_TEST(test_abs_negative_int) {
  ck_assert_int_eq(s21_abs(-1), abs(-1));
  ck_assert_int_eq(s21_abs(-17), abs(-17));
  ck_assert_int_eq(s21_abs(-26), abs(-26));
  ck_assert_int_eq(s21_abs(-50), abs(-50));
  ck_assert_int_eq(s21_abs(-96), abs(-96));
}
END_TEST

START_TEST(test_abs_specific_value) {
  ck_assert_int_eq(s21_abs(0), abs(0));
  ck_assert_int_eq(s21_abs(-0), abs(-0));
  ck_assert_int_eq(s21_abs(2147483647), abs(2147483647));
  ck_assert_int_eq(s21_abs(-2147483647), abs(-2147483647));
}
END_TEST

Suite *s21_test_abs() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_abs");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_abs_positive_int);
  tcase_add_test(tc_core, test_abs_negative_int);
  tcase_add_test(tc_core, test_abs_specific_value);

  suite_add_tcase(s, tc_core);
  return s;
}
