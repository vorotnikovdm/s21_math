#ifndef S21_TEST_MATH
#define S21_TEST_MATH

#include <check.h>
#include <math.h>
#include <stdlib.h>

#include "../s21_math.h"

Suite* s21_test_abs();
Suite* s21_test_acos();
Suite* s21_test_asin();
Suite* s21_test_atan();
Suite* s21_test_ceil();
Suite* s21_test_cos();
Suite* s21_test_exp();
Suite* s21_test_fabs();
Suite* s21_test_floor();
Suite* s21_test_fmod();
Suite* s21_test_log();
Suite* s21_test_pow();
Suite* s21_test_sin();
Suite* s21_test_sqrt();
Suite* s21_test_tan();

void run_test();

#endif