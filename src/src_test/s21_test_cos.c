#include "s21_test_math.h"

START_TEST(test_cos_positive_double) {
  ck_assert_ldouble_eq_tol(s21_cos(S21_PI), cosl(S21_PI), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_cos(S21_PI / 2), cosl(S21_PI / 2), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_cos(3 * S21_PI / 2), cosl(3 * S21_PI / 2),
                           S21_EPS);
  ck_assert_ldouble_eq_tol(s21_cos(2 * S21_PI), cosl(2 * S21_PI), S21_EPS);
}
END_TEST

START_TEST(test_cos_negative_double) {
  ck_assert_ldouble_eq_tol(s21_cos(-S21_PI), cosl(-S21_PI), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_cos(-S21_PI / 2), cosl(-S21_PI / 2), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_cos(-3 * S21_PI / 2), cosl(-3 * S21_PI / 2),
                           S21_EPS);
  ck_assert_ldouble_eq_tol(s21_cos(-2 * S21_PI), cosl(-2 * S21_PI), S21_EPS);
}
END_TEST

START_TEST(test_cos_specific_value) {
  ck_assert_ldouble_eq_tol(s21_cos(0.0), cosl(0.0), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_cos(-0.0), cosl(-0.0), S21_EPS);
  ck_assert(isnan(s21_cos(S21_NAN)) == 1);
  ck_assert(isnan(s21_cos(S21_INF)) == 1);
  ck_assert(isnan(s21_cos(S21_NINF)) == 1);
}
END_TEST

Suite *s21_test_cos() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_cos");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_cos_positive_double);
  tcase_add_test(tc_core, test_cos_negative_double);
  tcase_add_test(tc_core, test_cos_specific_value);

  suite_add_tcase(s, tc_core);
  return s;
}
