#include "s21_test_math.h"

START_TEST(test_exp_positive_int) {
  ck_assert_ldouble_eq_tol(s21_exp(1), exp(1), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_exp(17), exp(17), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_exp(26), exp(26), 1e-4);
  ck_assert_ldouble_eq_tol(s21_exp(35), exp(35), 1e6);
  ck_assert(isnan(s21_exp(96)) == 1);
}
END_TEST

START_TEST(test_exp_negative_int) {
  ck_assert_ldouble_eq_tol(s21_exp(-1), exp(-1), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_exp(-5), exp(-5), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_exp(-14), exp(-14), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_exp(-29), exp(-29), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_exp(-63), exp(-63), S21_EPS);
}
END_TEST

START_TEST(test_exp_positive_double) {
  ck_assert_ldouble_eq_tol(s21_exp(0.00034), exp(0.00034), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_exp(1.68234), exp(1.68234), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_exp(6.6432), exp(6.6432), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_exp(28.969), exp(28.969), 1e-3);
  ck_assert(isnan(s21_exp(74.972)) == 1);
}
END_TEST

START_TEST(test_exp_negative_double) {
  ck_assert_ldouble_eq_tol(s21_exp(-0.00001), exp(-0.00001), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_exp(-0.992347), exp(-0.992347), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_exp(-2.682), exp(-2.682), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_exp(-14.79635), exp(-14.79635), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_exp(-46.942), exp(-46.942), S21_EPS);
}
END_TEST

START_TEST(test_exp_specific_value) {
  ck_assert_ldouble_eq_tol(s21_exp(0.0), exp(0.0), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_exp(+0.0), exp(+0.0), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_exp(-0.0), exp(-0.0), S21_EPS);
  ck_assert(isnan(s21_exp(S21_NAN)) == 1);
  ck_assert_ldouble_eq(s21_exp(S21_INF), exp(S21_INF));
  ck_assert_ldouble_eq(s21_exp(S21_NINF), exp(S21_NINF));
}
END_TEST

Suite *s21_test_exp() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_exp");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_exp_positive_int);
  tcase_add_test(tc_core, test_exp_negative_int);
  tcase_add_test(tc_core, test_exp_positive_double);
  tcase_add_test(tc_core, test_exp_negative_double);
  tcase_add_test(tc_core, test_exp_specific_value);

  suite_add_tcase(s, tc_core);
  return s;
}
