#include "s21_test_math.h"

START_TEST(test_pow_positive_int) {
  ck_assert_ldouble_eq_tol(s21_pow(1, 2), powl(1, 2), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(5, 3), powl(5, 3), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(7, 2), powl(7, 2), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(1, -2), powl(1, -2), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(5, -3), powl(5, -3), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(7, -2), powl(7, -2), S21_EPS);
}
END_TEST

START_TEST(test_pow_negative_int) {
  ck_assert_ldouble_eq_tol(s21_pow(-1, 2), powl(-1, 2), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(-5, 3), powl(-5, 3), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(-7, 2), powl(-7, 2), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(-1, -2), powl(-1, -2), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(-5, -3), powl(-5, -3), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(-7, -2), powl(-7, -2), S21_EPS);
}
END_TEST

START_TEST(test_pow_positive_double) {
  ck_assert_ldouble_eq_tol(s21_pow(1.25, 2.13), powl(1.25, 2.13), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(5.55, 3.33), powl(5.55, 3.33), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(7.71, 2.21), powl(7.71, 2.21), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(1.25, -2.13), powl(1.25, -2.13), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(5.55, -3.33), powl(5.55, -3.33), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(7.71, -2.21), powl(7.71, -2.21), S21_EPS);
}
END_TEST

START_TEST(test_pow_negative_double) {
  ck_assert(isnan(s21_pow(-1.25, 2.21)) == 1);
  ck_assert(isnan(s21_pow(-5.55, 3.33)) == 1);
  ck_assert(isnan(s21_pow(-7.71, 2.21)) == 1);
  ck_assert(isnan(s21_pow(-1.25, -2.13)) == 1);
  ck_assert(isnan(s21_pow(-5.55, -3.33)) == 1);
  ck_assert(isnan(s21_pow(-7.71, -2.21)) == 1);
}

START_TEST(test_pow_different_value) {
  ck_assert_ldouble_eq_tol(s21_pow(1, 2.25), powl(1, 2.25), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(5.25, 3), powl(5.25, 3), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(1.15, -2.21), powl(1.15, -2.21), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(-7.21, -2), powl(-7.21, -2), S21_EPS);
}
END_TEST

START_TEST(test_pow_specific_value) {
  ck_assert_ldouble_eq_tol(s21_pow(1, 0), powl(1, 0), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_pow(-1, S21_INF), powl(-1, S21_INF), S21_EPS);
  ck_assert_ldouble_eq(s21_pow(-2.21, S21_INF), powl(-2.21, S21_INF));
  ck_assert_ldouble_eq(s21_pow(S21_NINF, 2.21), powl(S21_NINF, 2.21));
  ck_assert_ldouble_eq(s21_pow(S21_NINF, 2), powl(S21_NINF, 2));
  ck_assert_ldouble_eq_tol(s21_pow(-3, S21_NINF), powl(-3, S21_NINF), S21_EPS);
  ck_assert_ldouble_eq(s21_pow(S21_NINF, 4.21), powl(S21_NINF, 4.21));
  ck_assert_ldouble_eq_tol(s21_pow(S21_NINF, -4.21), powl(S21_NINF, -4.21),
                           S21_EPS);
  ck_assert_ldouble_eq(s21_pow(S21_NINF, 3.0), powl(S21_NINF, 3.0));
}
END_TEST

Suite *s21_test_pow() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_pow");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_pow_positive_int);
  tcase_add_test(tc_core, test_pow_negative_int);
  tcase_add_test(tc_core, test_pow_positive_double);
  tcase_add_test(tc_core, test_pow_negative_double);
  tcase_add_test(tc_core, test_pow_different_value);
  tcase_add_test(tc_core, test_pow_specific_value);

  suite_add_tcase(s, tc_core);
  return s;
}
