#include "s21_test_math.h"

START_TEST(test_atan_positive_double) {
  ck_assert_ldouble_eq_tol(s21_atan(1), atan(1), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(0.5), atan(0.5), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(0.25), atan(0.25), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(0.111), atan(0.111), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(0.15), atan(0.15), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(0.255), atan(0.255), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(0.25556), atan(0.25556), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(0.000125), atan(0.000125), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(0.999999), atan(0.999999), S21_EPS);
}
END_TEST

START_TEST(test_atan_negative_double) {
  ck_assert_ldouble_eq_tol(s21_atan(-1), atan(-1), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-0.5), atan(-0.5), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-0.25), atan(-0.25), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-0.111), atan(-0.111), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-0.15), atan(-0.15), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-0.255), atan(-0.255), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-0.25556), atan(-0.25556), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-0.000125), atan(-0.000125), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-0.999999), atan(-0.999999), S21_EPS);
}
END_TEST

START_TEST(test_atan_big_value) {
  ck_assert_ldouble_eq_tol(s21_atan(1000), atan(1000), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(10000), atan(10000), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(50000), atan(50000), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(60000), atan(60000), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(10000000), atan(10000000), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(1000.255), atan(1000.255), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(120.25556), atan(120.25556), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(55.000125), atan(55.000125), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(2.999999), atan(2.999999), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-1000), atan(-1000), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-10000), atan(-10000), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-50000), atan(-50000), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-60000), atan(-60000), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-10000000), atan(-10000000), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-1000.255), atan(-1000.255), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-120.25556), atan(-120.25556), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-55.000125), atan(-55.000125), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-2.999999), atan(-2.999999), S21_EPS);
}
END_TEST

START_TEST(test_atan_specific_value) {
  ck_assert_ldouble_eq_tol(s21_atan(0.0), atan(0.0), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-0.0), atan(-0.0), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(INFINITY), atan(INFINITY), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(-INFINITY), atan(-INFINITY), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(S21_INF), atan(S21_INF), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_atan(S21_NINF), atan(S21_NINF), S21_EPS);
}
END_TEST

Suite *s21_test_atan() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_atan");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_atan_positive_double);
  tcase_add_test(tc_core, test_atan_negative_double);
  tcase_add_test(tc_core, test_atan_big_value);
  tcase_add_test(tc_core, test_atan_specific_value);

  suite_add_tcase(s, tc_core);
  return s;
}
