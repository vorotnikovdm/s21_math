#include "s21_test_math.h"

START_TEST(test_sin_positive_double) {
  ck_assert_ldouble_eq_tol(s21_sin(S21_PI), sinl(S21_PI), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_sin(S21_PI / 2), sinl(S21_PI / 2), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_sin(3 * S21_PI / 2), sinl(3 * S21_PI / 2),
                           S21_EPS);
  ck_assert_ldouble_eq_tol(s21_sin(2 * S21_PI), sinl(2 * S21_PI), S21_EPS);
}
END_TEST

START_TEST(test_sin_negative_double) {
  ck_assert_ldouble_eq_tol(s21_sin(-S21_PI), sinl(-S21_PI), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_sin(-S21_PI / 2), sinl(-S21_PI / 2), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_sin(-3 * S21_PI / 2), sinl(-3 * S21_PI / 2),
                           S21_EPS);
  ck_assert_ldouble_eq_tol(s21_sin(-2 * S21_PI), sinl(-2 * S21_PI), S21_EPS);
}
END_TEST

START_TEST(test_sin_specific_value) {
  ck_assert_ldouble_eq_tol(s21_sin(0.0), sinl(0.0), S21_EPS);
  ck_assert_ldouble_eq_tol(s21_sin(-0.0), sinl(-0.0), S21_EPS);
  ck_assert(isnan(s21_sin(S21_NAN)) == 1);
  ck_assert(isnan(s21_sin(S21_INF)) == 1);
  ck_assert(isnan(s21_sin(S21_NINF)) == 1);
}
END_TEST

Suite *s21_test_sin() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_sin");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_sin_positive_double);
  tcase_add_test(tc_core, test_sin_negative_double);
  tcase_add_test(tc_core, test_sin_specific_value);

  suite_add_tcase(s, tc_core);
  return s;
}
