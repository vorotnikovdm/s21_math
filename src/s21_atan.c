#include "s21_math.h"

long double s21_atan(double x) {
  long double res = x, t = x;
  if (x == 1) {
    res = S21_PI_4;
  } else if (x == -1) {
    res = -S21_PI_4;
  } else if (x > 1) {
    res = S21_PI / 2 - s21_atan((1 / x));
  } else if (x < -1) {
    res = -(S21_PI / 2) - s21_atan((1 / x));
  } else if (1 > x && x > -1) {
    for (int n = 1; s21_fabs(t) > S21_EPS; ++n) {
      t *= -x * x * (2 * n - 1) / (2 * n + 1);
      res += t;
    }
  }
  return res;
}
