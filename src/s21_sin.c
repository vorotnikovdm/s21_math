#include "s21_math.h"

long double s21_sin(double x) {
  long double current_value = 0.0;
  int sign = (x < 0) ? -1 : 1;
  if (x == 0) {
    current_value = x;
  } else if (x == S21_INF || x == S21_NINF || x != x) {
    current_value = S21_NAN;
  } else if (s21_fabs(x) != (2 * S21_PI)) {
    x = s21_fmod(s21_fabs(x), 2 * S21_PI);
    if (x > S21_PI) {
      x -= S21_PI;
      sign *= -1;
    }
    if (x > S21_PI / 2) {
      x = S21_PI - x;
    }
    long double previous_value = 0.0;
    int i = 0, j = 1;
    long double factorial = 1.0;
    do {
      if (i != 0) {
        for (int q = j; q < (2 * i + 1); q++) {
          factorial = factorial + factorial * q;
        }
      }
      j = 2 * i + 1;
      current_value += (s21_pow(-1, i) * s21_pow(x, 2 * i + 1)) / factorial;
      i++;
      if (s21_fabs(current_value - previous_value) < S21_EPS || i > 500) {
        break;
      }
      previous_value = current_value;
    } while (1);
  } else
    current_value = -0.0;
  return current_value * sign;
}
