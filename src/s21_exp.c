#include "s21_math.h"

long double s21_exp(double x) {
  long double result = 0.0;
  if (x == S21_INF || x > 709) {
    result = S21_INF;
  } else if (x != x || x > 52) {
    result = S21_NAN;
  } else if (x == S21_NINF || x < -42) {
    result = 0.0;
  } else {
    long double tmp = 1.0, factorial = 1.0;
    int negative = 0;
    if (x < 0) {
      x = -x;
      negative = 1;
    }
    long double exponent = x;
    result = 1.0 + x;
    for (int i = 2; tmp > S21_EXP_EPS; i++) {
      factorial *= i;
      exponent *= x;
      tmp = exponent / factorial;
      result += tmp;
    }
    if (negative) {
      result = 1 / result;
    }
  }
  return result;
}
