#include "s21_math.h"

long double s21_sqrt(double x) {
  long double result = 0.0;
  double_num number;
  number.num = x;
  if (x != x || x < 0) {
    result = S21_NAN;
  } else if (x == S21_INF) {
    result = S21_INF;
  } else if (number.bits.s == 1 && x == 0) {
    result *= -1;
  } else if (x > 0) {
    double close_value = 0.1;
    if (x < 1) {
      for (double i = 0.1; i * i >= x; i /= 10) {
        close_value = i;
      }
    } else {
      for (double i = 0.0; i * i <= x; i++) {
        close_value = i;
      }
    }
    result = (x / close_value + close_value) / 2;
    for (int i = 0; i < 20; i++) {
      result = (x / result + result) / 2;
    }
  }
  return result;
}
