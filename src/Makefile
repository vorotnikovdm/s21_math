# Variables
CFLAGS = gcc -Wall -Werror -Wextra -std=c11
INFO = report.info
HTML = $(REPORT)/index.html
GCOV_FLAGS = -fprofile-arcs -ftest-coverage
EXCLUDE = --exclude '/usr/*' --exclude 'src_test/*'
UNAME = $(shell uname -s)
OS = -lcheck

ifeq ($(UNAME), Linux)
	OS = -lcheck -lm -lsubunit
endif

# Folders
TEST = src_test
REPORT = report
GCOV = gcov
OBJ = obj

# Executable files
S21_TEST = $(TEST)/s21_test
S21_GCOV = s21_gcov

# Source and objective library files
SRC_MATH = *.c
OBJ_MATH = *.o
LIB_MATH = s21_math.a

# Source test files
SRC_TEST = $(TEST)/*.c


# Targets
all : s21_math.a test gcov_report

s21_math.a :
	$(CFLAGS) -c $(SRC_MATH)
	ar rcs $(LIB_MATH) $(OBJ_MATH)
	mkdir $(OBJ)
	mv $(OBJ_MATH) $(OBJ)

test : s21_math.a
	$(CFLAGS) $(SRC_TEST) -o $(S21_TEST) $(LIB_MATH) $(OS)
	./$(S21_TEST)

# Commands for 'lcov'
#
# eval "$(/opt/goinfre/USERNAME/homebrew/bin/brew shellenv)"
# brew update --force --quiet
# chmod -R go-w "$(brew --prefix)/share/zsh"

gcov_report :
	$(CFLAGS) $(SRC_MATH) $(SRC_TEST) -o $(S21_GCOV) $(OS) $(GCOV_FLAGS)
	./$(S21_GCOV)
	lcov -c -d . -t "$(REPORT)" -o $(INFO) $(EXCLUDE)
	genhtml -o $(REPORT) $(INFO)
	rm *test*.gc* $(INFO)
	mkdir $(GCOV)
	mv *.gc* $(GCOV)

open :
	open $(HTML)

style :
	clang-format -n *.c *.h $(TEST)/*.c $(TEST)/*.h

valgrind :
	CK_FORK=no valgrind --leak-check=full $(S21_TEST)

rebuild :
	make clean
	make all

clean :
	rm -rf $(S21_TEST) $(S21_GCOV) $(REPORT) $(OBJ) $(GCOV) *.a *.o *.gc*

.PHONY :
	all clean rebuild s21_math.a test gcov_report style open valgrind
