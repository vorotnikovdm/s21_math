#include "s21_math.h"

long double s21_ceil(double x) {
  unsigned long long input = 0;
  long double res = x;
  memcpy(&input, &x, 8);
  int exponent = ((input >> 52) & 2047) - 1023;
  if (exponent < 0) {
    x = x > 0;
  } else if (exponent > 0) {
    int fractional_bits = 52 - exponent;
    if (fractional_bits > 0) {
      unsigned long long integral_mask = 0xffffffffffffffff << fractional_bits;
      unsigned long long output = input & integral_mask;
      memcpy(&x, &output, 8);
      if (x > 0 && output != input) ++x;
    }
  }
  res = x;
  return res;
}
