#include "s21_math.h"

long double s21_log(double x) {
  long double result = 0.0, tmp = 0.0;
  int exponent = 0;
  if (x != x || x < 0 || x == S21_NINF) {
    result = S21_NAN;
  } else if (x == S21_INF) {
    result = S21_INF;
  } else if (x == 0) {
    result = S21_NINF;
  } else {
    while (x >= S21_EXP) {
      x /= S21_EXP;
      exponent++;
    }
    for (int i = 0; i < 100; i++) {
      tmp = result;
      result = tmp + 2 * (x - s21_exp(tmp)) / (x + s21_exp(tmp));
    }
  }
  return result + exponent;
}
