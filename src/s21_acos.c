#include "s21_math.h"

long double s21_acos(double x) {
  long double res = x, temp = 0.0;
  if (x == 0) {
    res = S21_PI_2;
  } else if (x > 0) {
    temp = ((s21_sqrt(1 - x * x) / x));
    res = s21_atan(temp);
  } else if (x < 0) {
    temp = (s21_sqrt(1 - x * x) / x);
    res = s21_atan(temp) + S21_PI;
  }
  return res;
}
