#include "s21_math.h"

long double s21_floor(double x) {
  long double res = 0.0;
  if (x == S21_INF || x == S21_NINF) {
    res = x;
  } else {
    if (x < 0 && s21_fmod(x, 1) != 0) {
      res = x - (1 + s21_fmod(x, 1));
    } else
      res = x - s21_fmod(x, 1);
  }
  return res;
}
