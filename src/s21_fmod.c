#include "s21_math.h"

long double s21_fmod(double x, double y) {
  long double res = 0.0;
  double_num number;
  number.num = x;
  y = s21_fabs(y);
  x = s21_fabs(x);
  if (y != 0 && x != S21_INF && y == y) {
    while (x >= y) {
      x -= y;
    }
    if (number.bits.s == 1) {
      res = -x;
    } else {
      res = x;
    }
  } else {
    res = S21_NAN;
  }
  return res;
}
