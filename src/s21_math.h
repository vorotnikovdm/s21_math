#ifndef S21_MATH
#define S21_MATH

#include <string.h>

#define S21_NAN 0. / 0.
#define S21_INF 1. / 0.
#define S21_NINF -1. / 0.
#define S21_EPS 0.000001
#define S21_PI 3.14159265358979323846
#define S21_PI_2 1.57079632679489655800
#define S21_PI_4 0.78539816339744827900
#define S21_EXP 2.71828182845904523536
#define S21_EXP_EPS 0.0000000000000000001

typedef union {
  double num;
  struct {
    unsigned long m : 63;
    unsigned int s : 1;
  } bits;
} double_num;

int s21_abs(int x);               // модуль целого числа
long double s21_acos(double x);   // арккосинус
long double s21_asin(double x);   // арксинус
long double s21_atan(double x);   // арктангенс
long double s21_ceil(double x);   // округление вверх
long double s21_cos(double x);    // косинус
long double s21_exp(double x);    // экспонента в степени
long double s21_fabs(double x);   // модуль дробного числа
long double s21_floor(double x);  // округление вниз
long double s21_fmod(double x, double y);  // остаток от деления дробных чисел
long double s21_log(double x);                 // логарифм
long double s21_pow(double base, double exp);  // возведение в степень
long double s21_sin(double x);                 // синус
long double s21_sqrt(double x);  // квадратный корень
long double s21_tan(double x);   // тангенс

#endif
