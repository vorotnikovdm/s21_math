#include "s21_math.h"

long double s21_pow(double base, double e) {
  long double res = 0.0;
  if ((base == 1) || (e == 0) ||
      ((base == -1) &&
       ((e == S21_INF) || (e == S21_NINF) || (s21_fmod(e, 2) == 0))))
    res = 1;
  else if ((base == -1) && (s21_fmod(e, 2) == 1))
    res = -1;
  else if (((base < -1) && (e == S21_INF)) ||
           ((base == S21_NINF) && (e > 0) && (s21_fmod(e, 1) != 0)) ||
           ((base == S21_NINF) && (s21_fmod(e, 2) == 0) && (e > 0)))
    res = S21_INF;
  else if ((base == S21_NINF) && (e > 0) && (s21_fmod(e, 2) == 1))
    res = S21_NINF;
  else if (((base < -1) && (e == S21_NINF)) ||
           ((base == S21_NINF) && (s21_fabs(e) > 1)))
    res = 0;
  else if ((base < 0) && (s21_fmod(e, 1) != 0))
    res = S21_NAN;
  else {
    long double copy = base;
    if (copy < 0) {
      copy = -copy;
      res = s21_exp(e * s21_log(copy));
      if (s21_fmod(e, 2) != 0) {
        res = -res;
      } else {
        res = s21_exp(e * s21_log(copy));
      }
    } else {
      res = s21_exp(e * s21_log(base));
    }
  }
  return res;
}
